# rdf-FAIRify-Your-Dataset

## 1. Goal

The goal of this session is to give a general overview of the practical aspects of generating a FAIR-compliant version of an RDF dataset.


## 2. TODO

- [ ] add elements from DCAT
    - Data Integration for Open Data on the Web (https://link.springer.com/chapter/10.1007/978-3-319-61033-7_1)
- [ ] section expose your dataset and its VoID description
- [ ] section register your VoID description
- [ ] section example of using VoID descriptions of existing datasets
