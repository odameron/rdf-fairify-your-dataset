{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# FAIR-ify your RDF dataset"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Version:** 0.3\n",
    "\n",
    "The latest version of this document is available at: https://gitlab.com/odameron/rdf-fairify-your-dataset"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Objective \n",
    "The goal of this session is to give a general overview of the practical aspects of generating a FAIR-compliant version of a dataset."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Approach"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. start from a simple RDF dataset\n",
    "2. create a VoID description of your dataset\n",
    "3. **TODO** expose your dataset and its VoID description\n",
    "4. **TODO** register your VoID description\n",
    "5. **TODO** example of using VoID descriptions of existing datasets"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Reference\n",
    "\n",
    "- W3C [Describing Linked Datasets with the VoID Vocabulary](https://www.w3.org/TR/void/)\n",
    "- W3C [Data Catalog Vocabulary (DCAT) - Version 2](https://www.w3.org/TR/vocab-dcat-2/)\n",
    "- A user-friendly tool for generating a description of a dataset: [Metadatamatic](https://wimmics.github.io/voidmatic/)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4. Create an RDF dataset"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Step1:** Save the following in a `exampleDataset.ttl` file, and copy it to `exampleDataset-v1.ttl` so that `exampleDataset.ttl` is always the latest version, and you archive the successive versions.\n",
    "\n",
    "```turtle\n",
    "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n",
    "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n",
    "\n",
    "PREFIX ex: <http://www.example.org/>\n",
    "PREFIX bp: <http://www.biopax.org/release/biopax-level3.owl#>\n",
    "PREFIX ensembl: <http://rdf.ebi.ac.uk/resource/ensembl/>\n",
    "\n",
    "ex:gene1 rdf:type bp:Gene .\n",
    "ex:gene1 rdfs:label \"Gene1\" .\n",
    "ex:gene1 ex:similar_to ensembl:ENSG00000171105 .\n",
    "```\n",
    "\n",
    " ![Visual representation of exampleDataset.ttl](./exampleDataset.png) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 5. Create a VoID description of your dataset"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> ***Definition:*** **VoID** (Vocabulary of Interlinked Datasets)\n",
    "> \n",
    "> RDF Schema vocabulary for expressing metadata about RDF datasets.\n",
    "> - The namespace for the VoID vocabulary is `http://rdfs.org/ns/void#`\n",
    "> - A dataset is modelled as an instance of the `void:Dataset` class. This instance is a single RDF resource that represents the entire dataset, and thus allows us to easily make statements about the entire dataset and all its triples.\n",
    "\n",
    "> ***Definition:*** **dataset**\n",
    ">\n",
    "> A dataset is a set of RDF triples that are published, maintained or aggregated by a single provider.\n",
    "> Unlike RDF graphs, which are purely mathematical constructs, the term dataset has a social dimension: we think of a dataset as a meaningful collection of triples, that deal with a certain topic, originate from a certain source or process, are hosted on a certain server, or are aggregated by a certain custodian.\n",
    "\n",
    "> ***Definition:*** **link between datasets**\n",
    ">\n",
    "> An RDF link is an RDF triple whose subject and object are described in different datasets.\n",
    "\n",
    "> ***Definition:*** **linkset**\n",
    ">\n",
    "> A linkset is a collection of RDF links between two datasets. It is a set of RDF triples where all subjects are in one dataset and all objects are in another dataset.\n",
    "> - A dataset is modelled as an instance of the `void:Linkset` class.\n",
    "> - `void:Linkset` is a subclass of `void:Dataset`.\n",
    "> - NB: `rdf:type` statements are not considered links for the purposes of VoID, even though subject and object typically reside on different domains. VoiD has a dedicated mechanism for listing the classes used in a dataset (cf. `void:vocabulary` below). In our example, \n",
    ">     - there is a link to Ensembl\n",
    ">     - the dataset uses the vocabulary BioPAX\n",
    "\n",
    "\n",
    "4 main aspects:\n",
    "- **General metadata** following the Dublin Core model.\n",
    "- **Access metadata** describes how RDF data can be accessed using various protocols.\n",
    "- **Structural metadata** describes the structure and schema of datasets and is useful for tasks such as querying and data integration.\n",
    "- **Description of links between datasets** are helpful for understanding how multiple datasets are related and can be used together."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 5.0 Create the file containing the description of your dataset"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The VoID description of a dataset is usually stored in a file named `void.ttl` in the same directory as the dataset.\n",
    "\n",
    "**Step2:** create a `void.ttl` file in the same directory as your `exampleDataset.ttl` and containing the following prefix definitions:\n",
    "\n",
    "```turtle\n",
    "# Standard RDF prefixes\n",
    "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \n",
    "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \n",
    "PREFIX owl: <http://www.w3.org/2002/07/owl#> \n",
    "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> \n",
    "\n",
    "# VoID prefix (obviously)\n",
    "PREFIX void: <http://rdfs.org/ns/void#> \n",
    "\n",
    "# Prefixes of usual vocabularies used for describing our dataset\n",
    "PREFIX dcterms: <http://purl.org/dc/terms/> \n",
    "PREFIX foaf: <http://xmlns.com/foaf/0.1/> \n",
    "PREFIX pav: <http://purl.org/pav/> \n",
    "PREFIX sd: <http://www.w3.org/ns/sparql-service-description#> \n",
    "PREFIX wv: <http://vocab.org/waiver/terms/norms>   \n",
    "\n",
    "# Our prefix\n",
    "PREFIX ex: <http://www.example.org/>\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 5.1 General metadata"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Step3:** create an identifier for your dataset (the one from section 3), e.g. `ex:exampleDataset`, and use 0, 1 or more occurrences of the following properties:\n",
    "- `rdf:type` to make it an instance of `void:Dataset`\n",
    "- `foaf:homepage` to point to the URL of the (HTML) homepage of your dataset project. NB: write this URL as a resource, so probably like `<http://...>`\n",
    "- `dcterms:title` to describe (as a string) the name of your dataset, e.g. `\"My example dataset for experimenting with VoID\"`\n",
    "- `dcterms:description` a longer textual description (typically a paragraph) of your dataset\n",
    "- `dcterms:creator` to refer to the entities who created the dataset. For humans, their ORCID or their web page\n",
    "- `dcterms:contributor` to refer to entities contributed to the dataset even if they are not its creators\n",
    "- `dcterms:created` to state the date of creation of the initial version (obviously) of the dataset. It should follow the YYYY-MM-DD format and is typically data-typed as `xsd:date`. It remains the same over the successive versions of the dataset\n",
    "- `dcterms:modified` to state the date of the current version of the dataset\n",
    "- `dcterms:publisher` to refer to the publisher (e.g. the ORCID of the creators, or an organization), for which you can use the `foaf:mbox` to provide some contact information (`<mailto:firstname.lastname@company.org>`)\n",
    "- `dcterms:licence` to refer to the licence of your dataset (e.g. `<https://www.gnu.org/licenses/gpl-3.0>`, `<http://creativecommons.org/licenses/by-sa/3.0/>`, ...)\n",
    "- `dcterms:subject` to provide keyword(s) (typically use wikidata items, MeSH terms, or domain ontology concepts)\n",
    "- `void:feature` to represent the format(s) of your dataset (typically `<http://www.w3.org/ns/formats/Turtle>`)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Step4:** create a version-dependant identifier for your dataset (e.g. `ex:exampleDataset-v1.2`,\n",
    "- make it an instance of `void:Dataset`\n",
    "- use the `dcterms:isVersionOf` to point to your generic dataset\n",
    "- duplicate the title and description\n",
    "- duplicate or adapt if necessary the creator(s) and contributor(s)\n",
    "- set `dcterms:created` to the date of the release (probably posterior to the date of creation of the generic version of your dataset)\n",
    "- add a `dcterms:modified` property to the generic version of the dataset, which value is the same as the creation date of this version of the dataset\n",
    "- set `pav:version` to the version number (as a litteral, e.g. `\"1.3\"`)\n",
    "- set the publisher, licence, etc."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***Solution:***\n",
    "\n",
    "```turtle\n",
    "##### GENERAL METADATA\n",
    "\n",
    "### Generic version of the dataset\n",
    "ex:exampleDataset rdf:type void:Dataset .\n",
    "ex:exampleDataset foaf:homepage <https://gitlab.com/odameron/rdf-fairify-your-dataset> .\n",
    "ex:exampleDataset dcterms:title \"My example dataset for experimenting with VoID.\" .\n",
    "ex:exampleDataset dcterms:description \"Simple dataset to illustrate the practical aspects of generating a FAIR-compliant version of an RDF dataset.\" .\n",
    "ex:exampleDataset dcterms:creator <https://orcid.org/0000-0001-8959-7189> . # Olivier Dameron\n",
    "ex:exampleDataset dcterms:creator <https://orcid.org/0000-0002-3597-8557> . # Alban Gaignard\n",
    "ex:exampleDataset dcterms:created \"2023-01-17\"^^xsd:date .\n",
    "ex:exampleDataset dcterms:modified \"2023-01-18\"^^xsd:date .\n",
    "ex:exampleDataset dcterms:publisher <https://orcid.org/0000-0001-8959-7189> .\n",
    "ex:exampleDataset dcterms:licence <https://www.gnu.org/licenses/gpl-3.0> .\n",
    "ex:exampleDataset dcterms:subject <http://www.wikidata.org/entity/Q7939443> .  # VoID\n",
    "ex:exampleDataset dcterms:subject <http://www.wikidata.org/entity/Q29032648> . # FAIR data\n",
    "ex:exampleDataset dcterms:subject <http://www.wikidata.org/entity/Q54872> .    # RDF\n",
    "\n",
    "<https://orcid.org/0000-0001-8959-7189> rdf:type foaf:Person .\n",
    "<https://orcid.org/0000-0001-8959-7189> rdfs:label \"Olivier Dameron\" .\n",
    "<https://orcid.org/0000-0001-8959-7189> foaf:mbox <mailto:firstname.lastname@univ-rennes.fr> .\n",
    "\n",
    "### Version dependent dataset\n",
    "ex:exampleDataset-v1.1 rdf:type void:Dataset .\n",
    "ex:exampleDataset-v1.1 dcterms:isVersionOf ex:exampleDataset .\n",
    "ex:exampleDataset-v1.1 foaf:homepage <https://gitlab.com/odameron/rdf-fairify-your-dataset> .\n",
    "ex:exampleDataset-v1.1 dcterms:title \"My example dataset for experimenting with VoID.\" .\n",
    "ex:exampleDataset-v1.1 dcterms:description \"Simple dataset to illustrate the practical aspects of generating a FAIR-compliant version of an RDF dataset.\" .\n",
    "ex:exampleDataset-v1.1 dcterms:creator <https://orcid.org/0000-0001-8959-7189> . # Olivier Dameron\n",
    "ex:exampleDataset-v1.1 dcterms:creator <https://orcid.org/0000-0002-3597-8557> . # Alban Gaignard\n",
    "ex:exampleDataset-v1.1 dcterms:created \"2023-01-18\"^^xsd:date .\n",
    "ex:exampleDataset-v1.1 pav:version \"1.1\" .\n",
    "ex:exampleDataset-v1.1 dcterms:publisher <https://orcid.org/0000-0001-8959-7189> .\n",
    "ex:exampleDataset-v1.1 dcterms:licence <https://www.gnu.org/licenses/gpl-3.0> .\n",
    "ex:exampleDataset dcterms:subject <http://www.wikidata.org/entity/Q7939443> .  # VoID\n",
    "ex:exampleDataset dcterms:subject <http://www.wikidata.org/entity/Q29032648> . # FAIR data\n",
    "ex:exampleDataset dcterms:subject <http://www.wikidata.org/entity/Q54872> .    # RDF\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![General metadata of the VoID description of an RDF dataset](void-generalMetadata.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 5.2 Access metadata"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Step5:** Complete the previous description with the following relations\n",
    "- `void:sparqlEndpoint` (if applicable) points to the URL of the SPARQL endpoint exposing your dataset\n",
    "- `void:dataDump` the URL of the file (not the download webpage)\n",
    "- `void:rootResource` (optional) a particularly important and central resource in the dataset\n",
    "- `void:exampleResource` (optional) a representative resource"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***Solution:***\n",
    "\n",
    "```turtle\n",
    "##### ACCESS METADATA\n",
    "\n",
    "ex:exampleDataset rdf:type void:Dataset .\n",
    "ex:exampleDataset-v1.1 rdf:type void:Dataset .\n",
    "ex:exampleDataset-v1.1 dcterms:isVersionOf ex:exampleDataset .\n",
    "\n",
    "ex:exampleDataset void:sparqlEndpoint <http://genouest.org/XXX/sparql/> .\n",
    "ex:exampleDataset void:dataDump <https://gitlab.com/odameron/rdf-fairify-your-dataset/-/raw/main/exampleDataset.ttl> .\n",
    "ex:exampleDataset void:rootResource ex:Gene .\n",
    "ex:exampleDataset void:exampleResource ex:gene1 .\n",
    "\n",
    "ex:exampleDataset-v1.1 void:dataDump <https://gitlab.com/odameron/rdf-fairify-your-dataset/-/raw/main/exampleDataset-v1.1.ttl> .\n",
    "ex:exampleDataset-v1.1 void:rootResource ex:Gene .\n",
    "ex:exampleDataset-v1.1 void:exampleResource ex:gene1 .\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Access metadata of the VoID description of an RDF dataset](void-accessMetadata.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 5.3 Structural metadata"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Step6:** Complete the previous description with the following relations for describing the identifiers and the use of external classes\n",
    "- `void:uriSpace` a string indicating the default namespace (i.e. the part of the URI common to the resource identifiers). In our example, typically `\"http://www.example.org/\"`\n",
    "- `void:vocabulary` the RDFS vocabularies and OWL ontologies used to describes resources in the dataset"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Step7:** Complete the previous description with the following relations for providing a quantitative overview of the dataset (at this point, look at the dataset to fill the values)\n",
    "- `void:triples` the total number of triples\n",
    "- `void:entities` the total number of entities described in the dataset\n",
    "- `void:classes` the total number of distinct classes occurring in the dataset, i.e. as the object of an `rdf:type` property\n",
    "- `void:properties` the total number of distinct properties occurring in the dataset\n",
    "- `void:distinctSubjects`\n",
    "- `void:distinctObjects` \n",
    "- (optional) `void:classPartition` can be used to quantify the number of instances of the main classes\n",
    "- (optional) `void:propertyPartition` can be used to quantify the number of triples having the same predicate"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Step8:** Write SPARQL queries to compute the values from step 7"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***Solution:***\n",
    "\n",
    "```turtle\n",
    "##### STRUCTURAL METADATA\n",
    "\n",
    "ex:exampleDataset rdf:type void:Dataset .\n",
    "ex:exampleDataset-v1.1 rdf:type void:Dataset .\n",
    "ex:exampleDataset-v1.1 dcterms:isVersionOf ex:exampleDataset .\n",
    "\n",
    "ex:exampleDataset-v1.1 void:uriSpace \"http://www.example.org/\" . \n",
    "ex:exampleDataset-v1.1 void:vocabulary <http://www.biopax.org/release/biopax-level3.owl#> .\n",
    "ex:exampleDataset-v1.1 void:triples \"3\"^^xsd:integer .\n",
    "ex:exampleDataset-v1.1 void:entities \"1\"^^xsd:integer .\n",
    "ex:exampleDataset-v1.1 void:classes \"1\"^^xsd:integer .\n",
    "ex:exampleDataset-v1.1 void:classes \"3\"^^xsd:integer .\n",
    "ex:exampleDataset-v1.1 void:distinctSubjects \"1\"^^xsd:integer .\n",
    "ex:exampleDataset-v1.1 void:distinctObjects \"3\"^^xsd:integer .\n",
    "ex:exampleDataset-v1.1 void:classPartition [ void:class bp:Gene ; void:entities \"1\"^^xsd:integer ] .\n",
    "ex:exampleDataset-v1.1 void:propertyPartition [ void:property ex:similar_to ; void:entities \"1\"^^xsd:integer ] .\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Structural metadata of the VoID description of an RDF dataset](void-structuralMetadata.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 5.4 Links between datasets"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**TODO**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 5.5 General solution"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```turtle\n",
    "# Standard RDF prefixes\n",
    "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \n",
    "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \n",
    "PREFIX owl: <http://www.w3.org/2002/07/owl#> \n",
    "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> \n",
    "\n",
    "# VoID prefix (obviously)\n",
    "PREFIX void: <http://rdfs.org/ns/void#> \n",
    "\n",
    "# Prefixes of usual vocabularies used for describing our dataset\n",
    "PREFIX dcterms: <http://purl.org/dc/terms/> \n",
    "PREFIX foaf: <http://xmlns.com/foaf/0.1/> \n",
    "PREFIX pav: <http://purl.org/pav/> \n",
    "PREFIX sd: <http://www.w3.org/ns/sparql-service-description#> \n",
    "PREFIX wv: <http://vocab.org/waiver/terms/norms>   \n",
    "\n",
    "# Our prefix\n",
    "PREFIX ex: <http://www.example.org/>\n",
    "PREFIX bp: <http://www.biopax.org/release/biopax-level3.owl#>\n",
    "\n",
    "\n",
    "\n",
    "##### GENERAL METADATA\n",
    "\n",
    "### Generic version of the dataset\n",
    "ex:exampleDataset rdf:type void:Dataset .\n",
    "ex:exampleDataset foaf:homepage <https://gitlab.com/odameron/rdf-fairify-your-dataset> .\n",
    "ex:exampleDataset dcterms:title \"My example dataset for experimenting with VoID.\" .\n",
    "ex:exampleDataset dcterms:description \"Simple dataset to illustrate the practical aspects of generating a FAIR-compliant version of an RDF dataset.\" .\n",
    "ex:exampleDataset dcterms:creator <https://orcid.org/0000-0001-8959-7189> . # Olivier Dameron\n",
    "ex:exampleDataset dcterms:creator <https://orcid.org/0000-0002-3597-8557> . # Alban Gaignard\n",
    "ex:exampleDataset dcterms:created \"2023-01-17\"^^xsd:date .\n",
    "ex:exampleDataset dcterms:modified \"2023-01-18\"^^xsd:date .\n",
    "ex:exampleDataset dcterms:publisher <https://orcid.org/0000-0001-8959-7189> .\n",
    "ex:exampleDataset dcterms:licence <https://www.gnu.org/licenses/gpl-3.0> .\n",
    "ex:exampleDataset dcterms:subject <http://www.wikidata.org/entity/Q7939443> .  # VoID\n",
    "ex:exampleDataset dcterms:subject <http://www.wikidata.org/entity/Q29032648> . # FAIR data\n",
    "ex:exampleDataset dcterms:subject <http://www.wikidata.org/entity/Q54872> .    # RDF\n",
    "\n",
    "<https://orcid.org/0000-0001-8959-7189> rdf:type foaf:Person .\n",
    "<https://orcid.org/0000-0001-8959-7189> rdfs:label \"Olivier Dameron\" .\n",
    "<https://orcid.org/0000-0001-8959-7189> foaf:mbox <mailto:firstname.lastname@univ-rennes.fr> .\n",
    "\n",
    "### Version dependent dataset\n",
    "ex:exampleDataset-v1.1 rdf:type void:Dataset .\n",
    "ex:exampleDataset-v1.1 dcterms:isVersionOf ex:exampleDataset .\n",
    "ex:exampleDataset-v1.1 foaf:homepage <https://gitlab.com/odameron/rdf-fairify-your-dataset> .\n",
    "ex:exampleDataset-v1.1 dcterms:title \"My example dataset for experimenting with VoID.\" .\n",
    "ex:exampleDataset-v1.1 dcterms:description \"Simple dataset to illustrate the practical aspects of generating a FAIR-compliant version of an RDF dataset.\" .\n",
    "ex:exampleDataset-v1.1 dcterms:creator <https://orcid.org/0000-0001-8959-7189> . # Olivier Dameron\n",
    "ex:exampleDataset-v1.1 dcterms:creator <https://orcid.org/0000-0002-3597-8557> . # Alban Gaignard\n",
    "ex:exampleDataset-v1.1 dcterms:created \"2023-01-18\"^^xsd:date .\n",
    "ex:exampleDataset-v1.1 pav:version \"1.1\" .\n",
    "ex:exampleDataset-v1.1 dcterms:publisher <https://orcid.org/0000-0001-8959-7189> .\n",
    "ex:exampleDataset-v1.1 dcterms:licence <https://www.gnu.org/licenses/gpl-3.0> .\n",
    "ex:exampleDataset dcterms:subject <http://www.wikidata.org/entity/Q7939443> .  # VoID\n",
    "ex:exampleDataset dcterms:subject <http://www.wikidata.org/entity/Q29032648> . # FAIR data\n",
    "ex:exampleDataset dcterms:subject <http://www.wikidata.org/entity/Q54872> .    # RDF\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "##### ACCESS METADATA\n",
    "\n",
    "ex:exampleDataset rdf:type void:Dataset .\n",
    "ex:exampleDataset-v1.1 rdf:type void:Dataset .\n",
    "ex:exampleDataset-v1.1 dcterms:isVersionOf ex:exampleDataset .\n",
    "\n",
    "ex:exampleDataset void:sparqlEndpoint <http://genouest.org/XXX/sparql/> .\n",
    "ex:exampleDataset void:dataDump <https://gitlab.com/odameron/rdf-fairify-your-dataset/-/raw/main/exampleDataset.ttl> .\n",
    "ex:exampleDataset void:rootResource ex:Gene .\n",
    "ex:exampleDataset void:exampleResource ex:gene1 .\n",
    "\n",
    "ex:exampleDataset-v1.1 void:sparqlEndpoint <http://genouest.org/XXX/sparql/> .\n",
    "ex:exampleDataset-v1.1 void:dataDump <https://gitlab.com/odameron/rdf-fairify-your-dataset/-/raw/main/exampleDataset-v1.1.ttl> .\n",
    "ex:exampleDataset-v1.1 void:rootResource ex:Gene .\n",
    "ex:exampleDataset-v1.1 void:exampleResource ex:gene1 .\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "##### STRUCTURAL METADATA\n",
    "\n",
    "ex:exampleDataset rdf:type void:Dataset .\n",
    "ex:exampleDataset-v1.1 rdf:type void:Dataset .\n",
    "ex:exampleDataset-v1.1 dcterms:isVersionOf ex:exampleDataset .\n",
    "\n",
    "ex:exampleDataset void:uriSpace \"http://www.example.org/\" . \n",
    "ex:exampleDataset void:vocabulary <http://www.biopax.org/release/biopax-level3.owl#> .\n",
    "ex:exampleDataset void:triples \"3\"^^xsd:integer .\n",
    "ex:exampleDataset void:entities \"1\"^^xsd:integer .\n",
    "ex:exampleDataset void:classes \"1\"^^xsd:integer .\n",
    "ex:exampleDataset void:classes \"3\"^^xsd:integer .\n",
    "ex:exampleDataset void:distinctSubjects \"1\"^^xsd:integer .\n",
    "ex:exampleDataset void:distinctObjects \"3\"^^xsd:integer .\n",
    "ex:exampleDataset void:classPartition [ void:class bp:Gene ; void:entities \"1\"^^xsd:integer ] .\n",
    "ex:exampleDataset void:propertyPartition [ void:property ex:similar_to ; void:entities \"1\"^^xsd:integer ] .\n",
    "\n",
    "\n",
    "ex:exampleDataset-v1.1 void:uriSpace \"http://www.example.org/\" . \n",
    "ex:exampleDataset-v1.1 void:vocabulary <http://www.biopax.org/release/biopax-level3.owl#> .\n",
    "ex:exampleDataset-v1.1 void:triples \"3\"^^xsd:integer .\n",
    "ex:exampleDataset-v1.1 void:entities \"1\"^^xsd:integer .\n",
    "ex:exampleDataset-v1.1 void:classes \"1\"^^xsd:integer .\n",
    "ex:exampleDataset-v1.1 void:classes \"3\"^^xsd:integer .\n",
    "ex:exampleDataset-v1.1 void:distinctSubjects \"1\"^^xsd:integer .\n",
    "ex:exampleDataset-v1.1 void:distinctObjects \"3\"^^xsd:integer .\n",
    "ex:exampleDataset-v1.1 void:classPartition [ void:class bp:Gene ; void:entities \"1\"^^xsd:integer ] .\n",
    "ex:exampleDataset-v1.1 void:propertyPartition [ void:property ex:similar_to ; void:entities \"1\"^^xsd:integer ] .\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![VoID description of an RDF dataset](void.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 6. Expose your dataset"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 7. Register your VoID description"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
